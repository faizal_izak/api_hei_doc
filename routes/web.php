<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post("/register", "AuthController@register");
$router->post("/login", "AuthController@login");
$router->post("/logout", "AuthController@logout");

$router->get("/user", "UserController@index");

// Dokter
$router->get("/getDokter", "dokterController@getDokter");
$router->post("/postInsDokter", "dokterController@postInsDokter");
$router->post("/postUpdDokter", "dokterController@postUpdDokter");
$router->post("/postDelDokter", "dokterController@postDelDokter");

// Jadwal
$router->get("/getJadwal", "jadwalController@getJadwal");
$router->post("/postInsJadwal", "jadwalController@postInsJadwal");
$router->post("/postUpdJadwal", "jadwalController@postUpdJadwal");
$router->post("/postDelJadwal", "jadwalController@postDelJadwal");

// Rumah Sakit
$router->get("/getRumahsakit", "rumahsakitController@getRumahsakit");
$router->post("/postInsRumahsakit", "rumahsakitController@postInsRumahsakit");
$router->post("/postUpdRumahsakit", "rumahsakitController@postUpdRumahsakit");
$router->post("/postDelRumahsakit", "rumahsakitController@postDelRumahsakit");

// History
$router->get("/getHistory", "historyController@getHistory");
$router->post("/postDelHistory", "historyController@postDelHistory");


// Kota
$router->get("/getKota", "kotaController@getKota");
$router->post("/postInsKota", "kotaController@postInsKota");
$router->post("/postUpdKota", "kotaController@postUpdKota");
$router->post("/postDelKota", "kotaController@postDelKota");

// Spesialis
$router->get("/getSpesialis", "spesialisController@getSpesialis");
$router->post("/postInsSpesialis", "spesialisController@postInsSpesialis");
$router->post("/postUpdSpesialis", "spesialisController@postUpdSpesialis");
$router->post("/postDelSpesialis", "spesialisController@postDelSpesialis");


// Android


$router->post('/getDataRS', 'UserController@getDataRS');
$router->post('/getDataDokter', 'UserController@getDataDokter');
$router->post('/getNamaSpesialis', 'UserController@getNamaSpesialis');
$router->post('/getDataDokterSp', 'UserController@getDataDokterSp');
$router->post('/getDataDokterLengkap', 'UserController@getDataDokterLengkap');
$router->post('/getJadwalDokter', 'UserController@getJadwalDokter');
$router->post('/getNamaKota', 'UserController@getNamaKota');
$router->post('/getNamaKotaku', 'UserController@getNamaKotaku');
$router->post('/postInsHistory', 'UserController@postInsHistory');

// Route::get('/home', 'HomeController@index')->name('home');
// Route::post('/RS/getData', 'DataController@getDataRS')->name('getDataRS');
// Route::post('/dokter/getDataDokter', 'DataController@getDataDokter')->name('getDataDokter');
// Route::post('/RS/getNamaSpesialis', 'DataController@getNamaSpesialis')->name('getNamaSpesialis');
// Route::get('/dokter/getDataDokterSp', 'DataController@getDataDokterSp')->name('getDataDokterSp');
// Route::post('/dokter/getDataDokterLengkap', 'DataController@getDataDokterLengkap')->name('getDataDokterLengkap');
// Route::post('/dokter/getJadwalDokter', 'DataController@getJadwalDokter')->name('getJadwalDokter');