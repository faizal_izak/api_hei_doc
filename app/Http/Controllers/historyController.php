<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class historyController extends Controller
{
    function getHistory()
    {
        $query = DB::table("history")
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['keluhan'] = $value->keluhan;
            $data[$key]['nama_dokter'] = $value->nama_dokter;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
        }
        return response()->json($data);
    }

    function postDelHistory(Request $request)
    {
        // dd($request);
        $query = DB::table('history')->where('id', $request['id'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
