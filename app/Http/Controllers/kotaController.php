<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class kotaController extends Controller
{
    function getKota()
    {
        $query = DB::table("tb_kotas")
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama_kota'] = $value->nama_kota;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
        }
        return response()->json($data);
    }

    function postInsKota(Request $request)
    {
        $query = DB::table('tb_kotas')->insert([

            'nama_kota' => $request->nama_kota,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postUpdKota(Request $request)
    {
        $query = DB::table('tb_kotas')
            ->where('id', $request->id)
            ->update([
                'nama_kota' => $request->nama_kota,
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postDelKota(Request $request)
    {
        // dd($request);
        $query = DB::table('tb_kotas')->where('id', $request['id'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
