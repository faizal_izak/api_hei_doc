<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class jadwalController extends Controller
{
    function getJadwal()
    {
        $query = DB::table("tb_jadwal as jd")
            ->selectRaw('jd.*,dk.DOKTER_NAMA as NAMA_DOKTER')
            ->leftJoin('tb_dokter as dk', 'jd.DOKTER_ID', '=', 'dk.DOKTER_ID')
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['JADWAL_ID'] = $value->JADWAL_ID;
            $data[$key]['DOKTER_ID'] = $value->DOKTER_ID;
            $data[$key]['NAMA_DOKTER'] = $value->NAMA_DOKTER;
            $data[$key]['JADWAL_TGL'] = $value->JADWAL_TGL;
            $data[$key]['JADWAL_JAM_M'] = $value->JADWAL_JAM_M;
            $data[$key]['JADWAL_JAM_S'] = $value->JADWAL_JAM_S;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
        }
        return response()->json($data);
    }

    function postInsJadwal(Request $request)
    {
        $query = DB::table('tb_jadwal')->insert([

            'DOKTER_ID' => $request->DOKTER_ID,
            'JADWAL_TGL' => $request->JADWAL_TGL,
            'JADWAL_JAM_M' => $request->JADWAL_JAM_M,
            'JADWAL_JAM_S' => $request->JADWAL_JAM_S,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postUpdJadwal(Request $request)
    {
        $query = DB::table('tb_jadwal')
            ->where('JADWAL_ID', $request->JADWAL_ID)
            ->update([

                'DOKTER_ID' => $request->DOKTER_ID,
                'JADWAL_TGL' => $request->JADWAL_TGL,
                'JADWAL_JAM_M' => $request->JADWAL_JAM_M,
                'JADWAL_JAM_S' => $request->JADWAL_JAM_S,
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }

    function postDelJadwal(Request $request)
    {
        // dd($request);
        $query = DB::table('tb_jadwal')->where('JADWAL_ID', $request['JADWAL_ID'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
