<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //RS    
    public function getDataRS(Request $request)
    {

        $nm_rs = $request['nama_rs'];
        $data = DB::table('tb_rs')
            ->select(DB::raw("tb_rs.RS_NAMA,
        tb_rs.RS_ALAMAT,
        tb_rs.RS_TELP,
        tb_rs.RS_PROFIL,
        tb_kotas.nama_kota,
        CONCAT('http://192.168.43.188/alodocCoba/public/gb_rs/',tb_rs.RS_GBR) AS 'url'"))
            // ->where('tb_rs.RS_NAMA', 'like', "%" . $nm_rs . "%")
            ->leftjoin("tb_kotas", "tb_kotas.id", "=", "tb_rs.id")
            ->get();

        return $data;
    }

    //Spesialis 

    public function getNamaKota(Request $request)
    {
        $nm_kt = $request['nama_kota'];
        $namakota = DB::select("SELECT a.*, nama_kota
         FROM tb_rs a, tb_kotas b 
         WHERE a.id = b.id
         AND nama_kota = ?", [$nm_kt]);
        return $namakota;
    }

    public function getNamaSpesialis()
    {
        $dataSp = DB::select("SELECT SP_NAMA from tb_spesialis order by SP_NAMA desc;");
        return $dataSp;
    }
    public function getNamaKotaku()
    {
        $dataKota = DB::select("SELECT nama_kota from tb_kotas order by nama_kota desc;");
        return $dataKota;
    }
    //Dokter    
    public function getDataDokter(Request $request)
    {

        $nm_rs = $request['nama_rs'];
        $data = \DB::table('tb_dokter')
            ->select(DB::raw("DOKTER_NAMA,SP_NAMA as nama_sp,DOKTER_PROFIL,DOKTER_HP,DOKTER_STR,
            CONCAT('http://192.168.43.188/alodocCoba/public/gb_dokter/',DOKTER_GBR) AS 'DOKTER_GBR',
            CONCAT('http://192.168.43.188/alodocCoba/public/gb_rs/',RS_GBR) AS 'url',RS_NAMA"))
            ->leftjoin('tb_rs', 'tb_dokter.RS_ID', '=', 'tb_rs.id')
            ->join('tb_spesialis', 'tb_dokter.SP_ID', '=', 'tb_spesialis.id')
            ->where('RS_NAMA', $nm_rs)
            ->get();
        return $data;
    }

    public function getDataDokterSp(Request $request)
    {
        $nm_sp = $request['nama_sp'];
        $dataDokSp = DB::table('tb_dokter as td')
            ->select(DB::raw("
            td.DOKTER_NAMA,
            ts.SP_NAMA as nama_sp,
            td.DOKTER_PROFIL,
            td.DOKTER_HP,
            td.DOKTER_STR,
        CONCAT('http://192.168.43.188/alodocCoba/public/gb_dokter/',td.DOKTER_GBR) AS 'DOKTER_GBR',
        RS_NAMA,CONCAT('http://192.168.43.188/alodocCoba/public/gb_rs/',trs.RS_GBR) AS 'url'"))
            ->orwhere('SP_NAMA', "=", $nm_sp)
            ->leftjoin('tb_rs as trs', 'td.RS_ID', '=', 'trs.id')
            ->leftjoin('tb_spesialis as ts', 'td.SP_ID', '=', 'ts.id')
            ->get();
        return $dataDokSp;
    }

    public function getDataDokterLengkap(Request $request)
    {
        $nm_dok = $request['nama_dokter'];
        $dataDoklkp = DB::select("SELECT DOKTER_ID,
        CONCAT('http://192.168.43.188/alodocCoba/public/gb_dokter/',DOKTER_GBR) AS 'DOKTER_GBR',
        CONCAT('http://192.168.43.188/alodocCoba/public/gb_rs/',RS_GBR) AS 'url',
        DOKTER_NAMA,RS_NAMA,SP_NAMA,DOKTER_PROFIL,DOKTER_HP,DOKTER_STR 
        FROM tb_dokter d, tb_spesialis s, tb_rs r
        WHERE d.`SP_ID` = s.id
        AND d.`RS_ID` = r.`ID`
        and DOKTER_NAMA = ?", [$nm_dok]);
        return $dataDoklkp;
    }

    public function getJadwalDokter(Request $request)
    {
        $nm_dok = $request['nama_dokter'];
        $jadwalDok = DB::select("SELECT DOKTER_NAMA, JADWAL_JAM_M,JADWAL_JAM_S
         FROM tb_jadwal a, tb_dokter b 
         WHERE a.DOKTER_ID = b.DOKTER_ID
         AND DOKTER_NAMA = ?", [$nm_dok]);
        return $jadwalDok;
    }

    public function postInsHistory(Request $request)
    {
        $history = DB::table('history')->insert([
            'nama' => $request['nama'],
            'keluhan' => $request['keluhan'],
            'nama_dokter' => $request['nama_dokter'],
        ]);
        return $history;
    }
}
