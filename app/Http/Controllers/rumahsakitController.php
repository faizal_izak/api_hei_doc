<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class rumahsakitController extends Controller
{
    function getRumahsakit()
    {
        $query = DB::table("tb_rs as rs")
            ->selectRaw('rs.*, kt.nama_kota as namakota')
            ->leftJoin('tb_kotas as kt', 'rs.RS_KOTA', '=', 'kt.id')
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['RS_NAMA'] = $value->RS_NAMA;
            $data[$key]['RS_ALAMAT'] = $value->RS_ALAMAT;
            $data[$key]['RS_TELP'] = $value->RS_TELP;
            $data[$key]['RS_PROFIL'] = $value->RS_PROFIL;
            $data[$key]['RS_GBR'] = $value->RS_GBR;
            $data[$key]['namakota'] = $value->namakota;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
            $data[$key]['RS_KOTA'] = $value->RS_KOTA;
        }
        return response()->json($data);
    }

    function postInsRumahsakit(Request $request)
    {
        $query = DB::table('tb_rs')->insert([

            'RS_NAMA' => $request->RS_NAMA,
            'RS_ALAMAT' => $request->RS_ALAMAT,
            'RS_TELP' => $request->RS_TELP,
            'RS_PROFIL' => $request->RS_PROFIL,
            'RS_GBR' => $request->RS_GBR,
            'RS_KOTA' => $request->RS_KOTA,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }

    function postUpdRumahsakit(Request $request)
    {
        $query = DB::table('tb_rs')
            ->where('id', $request->id)
            ->update([

                'RS_NAMA' => $request->RS_NAMA,
                'RS_ALAMAT' => $request->RS_ALAMAT,
                'RS_TELP' => $request->RS_TELP,
                'RS_PROFIL' => $request->RS_PROFIL,
                'RS_GBR' => $request->RS_GBR,
                'RS_KOTA' => $request->RS_KOTA,
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postDelRumahsakit(Request $request)
    {
        // dd($request);
        $query = DB::table('tb_rs')->where('id', $request['id'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
