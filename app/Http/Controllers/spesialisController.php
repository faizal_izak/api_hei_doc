<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class spesialisController extends Controller
{
    function getSpesialis()
    {
        $query = DB::table("tb_spesialis")
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['SP_NAMA'] = $value->SP_NAMA;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
        }
        return response()->json($data);
    }

    function postInsSpesialis(Request $request)
    {
        $query = DB::table('tb_spesialis')->insert([

            'SP_NAMA' => $request->SP_NAMA,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postUpdSpesialis(Request $request)
    {
        $query = DB::table('tb_spesialis')
            ->where('id', $request->id)
            ->update([

                'SP_NAMA' => $request->SP_NAMA,
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postDelSpesialis(Request $request)
    {
        // dd($request);
        $query = DB::table('tb_spesialis')->where('id', $request['id'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
