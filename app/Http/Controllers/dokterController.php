<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class dokterController extends Controller
{
    function getDokter()
    {
        $query = DB::table("tb_dokter as dk")
            ->selectRaw('dk.*,sp.SP_NAMA as NAMA_SPESIALIS, rs.RS_NAMA as NAMA_RM')
            ->leftJoin('tb_rs as rs', 'rs.id', '=', 'dk.RS_ID')
            ->leftJoin('tb_spesialis as sp', 'dk.SP_ID', '=', 'sp.id')
            ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['DOKTER_ID'] = $value->DOKTER_ID;
            $data[$key]['NAMA_RM'] = $value->NAMA_RM;
            $data[$key]['NAMA_SPESIALIS'] = $value->NAMA_SPESIALIS;
            $data[$key]['RS_ID'] = $value->RS_ID;
            $data[$key]['SP_ID'] = $value->SP_ID;
            $data[$key]['DOKTER_NAMA'] = $value->DOKTER_NAMA;
            $data[$key]['DOKTER_PROFIL'] = $value->DOKTER_PROFIL;
            $data[$key]['DOKTER_HP'] = $value->DOKTER_HP;
            $data[$key]['DOKTER_STR'] = $value->DOKTER_STR;
            $data[$key]['DOKTER_GBR'] = $value->DOKTER_GBR;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['updated_at'] = $value->updated_at;
        }
        return response()->json($data);
    }

    function postInsDokter(Request $request)
    {
        $query = DB::table('tb_dokter')->insert([

            'RS_ID' => $request->RS_ID,
            'SP_ID' => $request->SP_ID,
            'DOKTER_NAMA' => $request->DOKTER_NAMA,
            'DOKTER_PROFIL' => $request->DOKTER_PROFIL,
            'DOKTER_HP' => $request->DOKTER_HP,
            'DOKTER_GBR' => $request->DOKTER_GBR,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }
    function postUpdDokter(Request $request)
    {
        $query = DB::table('tb_dokter')
            ->where('DOKTER_ID', $request->DOKTER_ID)
            ->update([

                'RS_ID' => $request->RS_ID,
                'SP_ID' => $request->SP_ID,
                'DOKTER_NAMA' => $request->DOKTER_NAMA,
                'DOKTER_PROFIL' => $request->DOKTER_PROFIL,
                'DOKTER_HP' => $request->DOKTER_HP,
                'DOKTER_GBR' => $request->DOKTER_GBR,
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        if ($query == true) {
            $data['code'] = "100";
            $data['message'] = "Sukses Simpan Data";
        } else {
            $data['code'] = "404";
            $data['message'] = "tidak berhasil";
        }
        return response()->json($data);
    }

    function postDelDokter(Request $request)
    {
        // dd($request);
        $query = DB::table('tb_dokter')->where('DOKTER_ID', $request['DOKTER_ID'])->delete();
        $data['code'] = "100";
        $data['message'] = "Sukses Hapus Data";
        return response()->json($data);
    }
}
